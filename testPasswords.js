$( document ).ready(function() {
   var indx = 0, // default
        iChars = 0, // default
        lenCg = 0, // default
        sdlenCg = -1, // default
        numberChanged = 0, // default
        indxNumbers = 1, // default
        typePass = 'characters', // characters, string
        limitPass = 6,
        usernameChar = 'truongcongsang',
        passwordChar = '',
        beforeCharacters = 'qqqqq', // 6-1=5 letters q for limitPass=6
        lastCharacters = 'q',
        // charaters = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m'],
        charaters = ['1', '2', '3'], // test characters
        lengthCharacters = charaters.length , // length tinh tu 1
        simplePasses = ['qwerty', '123456', '12345678', '123456789', 'abc123', '696969', '000000', '00000000', '000000000', '111111', '123123', '11111111', '111111111', 'password', 'iloveyou', 'welcome', 'passw0rd','dragon','password1', 'password2', 'password3', 'password4', 'password5', 'password6', 'khongmatkhau', 'hoilamchi', 'matkhau', 'anhnhoem','emnhoanh'];

    var refreshIntervalId = setInterval(function() {
            /*
            Search password is characters
            We will increase follow left(q) to right(m)
            */
            if (typePass === 'characters') {
                    // Neu Chuoi chua chay het cac characters con lai thi quet het den khi sdlenCg
                    if (indx != 0) {
                        if (indx%lengthCharacters == 0 && sdlenCg > 0) {
                            if (indxNumbers == lengthCharacters) {
                                indxNumbers = 1; // default
                                sdlenCg--;
                            } else {
                                lastCharacters = charaters[indxNumbers];
                                beforeCharacters = beforeCharacters.substring(0, (sdlenCg-1)) + lastCharacters + beforeCharacters.substring(sdlenCg);
                                
                                console.log ("Before Characters:" + beforeCharacters);
                                console.log ("sdlenCg:" + sdlenCg + ", numberChanged:" + numberChanged + ", lenCg:" + lenCg + ", indxNumbers:" + indxNumbers + ", lengthCharacters:" + lengthCharacters);
                                indxNumbers++;
                            }
                        } else {                      
                            // so lan het 1 vong = chieu dai characters
                            if (numberChanged == lengthCharacters) {
                                beforeCharacters =  beforeCharacters.replace('q', charaters[0]);
                                beforeCharacters =  beforeCharacters.replace(/3/g, charaters[0]); // input characters end.
                                numberChanged = 0;
                            } else {
                                beforeCharacters = beforeCharacters.substring(0, lenCg) + charaters[numberChanged] + beforeCharacters.substring(lenCg+1);                                
                                lenCg++;
                                numberChanged++;
                            }
                            sdlenCg = lenCg;
                        }
                    }

                    if (indx%lengthCharacters == 0) {
                        passwordChar = charaters[0] + beforeCharacters;
                    } else if (charaters[(indx-1)%lengthCharacters] === charaters[lengthCharacters-1]) {
                        passwordChar = charaters[lengthCharacters-1] + beforeCharacters;
                    } else {
                        passwordChar = charaters[indx%lengthCharacters] + beforeCharacters;
                    }
                    
                    // check tang limit pass
                    // ??? limitPass++;
                    /////////                    
                
                indx++;
            }
            passwordChar = passwordChar.slice(-limitPass);
            /* test */      
            
            if (indx == 50) {
                clearInterval(refreshIntervalId);
            }            
            console.log ('passwordChar:' + passwordChar);
            console.log ('-----------------'); 
    }, 1000);
});