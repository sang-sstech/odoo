$( document ).ready(function() {
   var indx = 0, // default
        iChars = 0, // default
        iB = 0,  // default
        permArr = [], // default
        usedChars = [], // default
        arrPermute = [],
        typePass = 'characters', // characters, string
        limitPass = 6,
        usernameChar = 'truongcongsang',
        passwordChar = '',
        b = 'b1', // default
        // charaters = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m'],
        charaters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'e', 'f', 'g', 'h'], // test characters
        lengthCharacters = charaters.length - 1, // length tinh tu 1
        simplePasses = ['qwerty', '123456', '12345678', '123456789', 'abc123', '696969', '000000', '00000000', '000000000', '111111', '123123', '11111111', '111111111', 'password', 'iloveyou', 'welcome', 'passw0rd','dragon','password1', 'password2', 'password3', 'password4', 'password5', 'password6', 'khongmatkhau', 'hoilamchi', 'matkhau', 'anhnhoem','emnhoanh'];

    function permute(input) {
        var i, ch;
        for (i = 0; i < input.length; i++) {
            ch = input.splice(i, 1)[0];
            usedChars.push(ch);
            if (input.length == 0) {
                permArr.push(usedChars.slice());
            }
            permute(input);
            input.splice(i, 0, ch);
            usedChars.pop();
        }
        return permArr;
    };
    /* 
    B1: Khởi tạo mang cho 6 phan tu don.(Không trùng nhau) 012345, 123456...
    B2: Khoi tao mang cho 1 phan tu don. 000000, 100000, 200000...121111...
    B3: Khoi tao mang cho 2 phan tu don
    B4: KHoi tao mang cho 3 phan tu don
    B5: Khoi tao mng cho 4 phan tu don
    B6: Khoi tao mang cho 5 phan tu don
    B7: Khoi tao mng cho 2 phan tu kep(11, 22...)
    B8: Khoi tao mang cho 3 phan tu kep
    B9: Khoi tao mang cho 4 phan tu kep
    B10: Khoi tao mang cho 5 phan tu kep
    B11: Khoi tao mang cho 6 phan tu kep(Khong hoan vi)
    */
    var refreshIntervalId = setInterval(function() {            
            if (typePass === 'characters') {
                var arrPass = [];
                /* B1
                    - iChars = 0: Duyệt mảng từ 0-5(limitPass=6)
                    - iChars = 1: Duyệt mảng từ 1-6(limitPass=6)...
                    - Nếu iChars < limitChar-6 trở đi thì lấy lại phần tử đầu tiên
                */                
                if (b === 'b1') {
                    if (arrPermute.length == 0) {
                        for (i=0; i<limitPass; i++) {
                            arrPass.push(charaters[(iChars+i)%lengthCharacters]);
                        }                        
                        iChars++;
                        arrPermute = permute(arrPass);
                    }
                    passwordChar = arrPermute.pop().join('');
                    // Kết thúc B1
                    if (iChars >= lengthCharacters) { 
                        b = 'b2';
                        iChars = 0;
                        arrPermute = [];
                        console.log ('End B1');
                    }
                }                
                ////////////////////////////////////////////////
                /* B2 continue */ 
                if (b === 'b2') {
                    for (i=0; i<limitPass; i++) {
                        arrPass.push(charaters[iChars]);
                    }                    
                    if (iChars == lengthCharacters) {
                        
                    }
                    if (iB == lengthCharacters) {
                        ib = 0;
                        iChars++;
                    }
                    iB++;
                }
            }

                // check tang limit pass
                // ??? limitPass++;
                /////////

            // Chỉ để điếm và dừng Interval         
            indx++;
            if (indx == 20) {
                clearInterval(refreshIntervalId);
            }
    }, 1000);
});